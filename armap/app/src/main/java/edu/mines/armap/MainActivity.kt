/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.mines.armap

import android.annotation.SuppressLint
import android.app.PendingIntent.getActivity
import android.content.IntentSender
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.media.Image
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.Gravity.*
import android.view.LayoutInflater
import android.widget.Button
import android.widget.FrameLayout
import android.widget.PopupWindow
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.ar.core.*
import com.google.ar.core.Point.OrientationMode
import edu.mines.armap.helpers.CameraPermissionHelper
import edu.mines.armap.helpers.DisplayRotationHelper
import edu.mines.armap.helpers.FullScreenHelper
import edu.mines.armap.helpers.SnackbarHelper
import edu.mines.armap.helpers.TapHelper
import edu.mines.armap.rendering.BackgroundRenderer
import edu.mines.armap.rendering.ObjectRenderer
import edu.mines.armap.rendering.ObjectRenderer.BlendMode
import edu.mines.armap.rendering.PlaneRenderer
import edu.mines.armap.rendering.PointCloudRenderer
import com.google.ar.core.exceptions.CameraNotAvailableException
import com.google.ar.core.exceptions.UnavailableApkTooOldException
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException
import com.google.ar.core.exceptions.UnavailableSdkTooOldException
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException
import edu.mines.armap.rendering.AlignPhoto
import java.io.IOException
import java.lang.Math.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import java.time.Instant
import kotlin.collections.ArrayList
import kotlin.math.PI
import kotlin.math.sin
import kotlin.math.cos

var canvasWidth = 0
var canvasHight = 0

fun axisAngleToPose(x: Float, y: Float, z: Float, angle: Float): Pose {
    val s = sin(angle / 2)
    return Pose.makeRotation(s * x, s * y, s * z, cos(angle / 2))
}

/**
 * This is a simple example that shows how to create an augmented reality (AR) application using the
 * ARCore API. The application will display any detected planes and will allow the user to tap on a
 * plane to place a 3d model of the Android robot.
 */
class MainActivity : AppCompatActivity(), GLSurfaceView.Renderer {

    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val TIME_DELTA = 5
    private val PLAY_SERVICES_RESOLUTION_REQUEST: Int = 5555
    private val REQUEST_CHECK_SETTINGS = 1

    // Rendering. The Renderers are created here, and initialized when the GL surface is created.
    private var surfaceView: GLSurfaceView? = null

    private var installRequested: Boolean = false

    private var session: Session? = null
    private val messageSnackbarHelper = SnackbarHelper()
    private var displayRotationHelper: DisplayRotationHelper? = null
    private var tapHelper: TapHelper? = null

    private val backgroundRenderer = BackgroundRenderer()
    private val mapObject = ObjectRenderer()
    private val pinObject = ObjectRenderer()
    private val pinShadowObject = ObjectRenderer()
    private val markObject = ObjectRenderer()
    private val planeRenderer = PlaneRenderer()
    private val pointCloudRenderer = PointCloudRenderer()

    // Temporary matrix allocated here to reduce number of allocations for each frame.
    private val tmpMatrix = FloatArray(16)

    // Anchors created from taps used for object placing.
    private val anchors = ArrayList<Anchor>()
    private var tmpAnchor: Anchor? = null

    private var lastSample: Instant? = null

    private var currentLocation: Location? = null

    private var wallSolver: WallSolver = WallSolver()

    private var mapFilename: String? = null

    private var reversed = false
    private var extraRotate = 0f

    var matricies: ArrayList<Matrix> = ArrayList()


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Figure out which map to load
        mapFilename = intent.extras.get("map_filename") as String
        Log.i("map_fn", mapFilename)

        setContentView(R.layout.activity_main)
        surfaceView = findViewById(R.id.surfaceview)
        displayRotationHelper = DisplayRotationHelper(this)

        // Set up tap listener.
        tapHelper = TapHelper(this)
        surfaceView!!.setOnTouchListener(tapHelper)

        // Set up renderer.
        surfaceView!!.preserveEGLContextOnPause = true
        surfaceView!!.setEGLContextClientVersion(2)
        // Alpha used for plane blending.
        surfaceView!!.setEGLConfigChooser(8, 8, 8, 8, 16, 0)
        surfaceView!!.setRenderer(this)
        surfaceView!!.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Modify the code inside the for loop to get new locations.
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    // There's a function isBetterLocation at the end of the file that you can
                    // call that'll compare two locations.
                    if(isBetterLocation(location, currentLocation)) {
                        currentLocation = location
                    }


                    Log.i("info_app_location", "Got location: " + location.toString())
                }
            }
        }

        if(checkPlayServices()) {
            Log.i("info_app_location", "Google Play services are available.")
            startLocationUpdates()
        } else {
            Log.i("info_app_location", "Google Play services not available.")
        }

        Log.i("info_app_location", "Google Play services not available.")

        Log.i("stuff", "D is ${builtinMapXY(  37.485725, -122.147213)}")

        installRequested = false

//        wallSolver.setupBuiltin()
    }

    private fun startLocationUpdates() {
        var locationRequest = createLocationRequest()

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
        builder.setAlwaysShow(true);

        val client: SettingsClient = LocationServices.getSettingsClient(this)

        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { locationSettingsResponse ->
            Log.i("info_app_location", "Settings task success.")
            fusedLocationClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null /* Looper */)
        }

        task.addOnFailureListener { exception ->
            Log.i("info_app_location", "Settings task failed.")
            if (exception is ResolvableApiException){
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.s
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(this@MainActivity,
                            REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    fun createLocationRequest(): LocationRequest {
        val locationRequest = LocationRequest().apply {
            interval = 1
            fastestInterval = 5
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        return locationRequest
    }

    override fun onResume() {
        super.onResume()

//        startLocationUpdates()

        if (session == null) {
            var exception: Exception? = null
            var message: String? = null
            try {
                when (ArCoreApk.getInstance().requestInstall(this, !installRequested)) {
                    ArCoreApk.InstallStatus.INSTALL_REQUESTED -> {
                        installRequested = true
                        return
                    }
                    ArCoreApk.InstallStatus.INSTALLED -> {
                    }
                }

                // ARCore requires camera permissions to operate. If we did not yet obtain runtime
                // permission on Android M and above, now is a good time to ask the user for it.
                if (!CameraPermissionHelper.hasCameraPermission(this)) {
                    CameraPermissionHelper.requestCameraPermission(this)
                    return
                }

                // Create the session.
                session = Session(/* context= */this)

            } catch (e: UnavailableArcoreNotInstalledException) {
                message = "Please install ARCore"
                exception = e
            } catch (e: UnavailableUserDeclinedInstallationException) {
                message = "Please install ARCore"
                exception = e
            } catch (e: UnavailableApkTooOldException) {
                message = "Please update ARCore"
                exception = e
            } catch (e: UnavailableSdkTooOldException) {
                message = "Please update this app"
                exception = e
            } catch (e: UnavailableDeviceNotCompatibleException) {
                message = "This device does not support AR"
                exception = e
            } catch (e: Exception) {
                message = "Failed to create AR session"
                exception = e
            }

            if (message != null) {
                messageSnackbarHelper.showError(this, message)
                Log.e(TAG, "Exception creating session", exception)
                return
            }
        }

        // Note that order matters - see the note in onPause(), the reverse applies here.
        try {
            session!!.resume()
        } catch (e: CameraNotAvailableException) {
            // In some cases (such as another camera app launching) the camera may be given to
            // a different app instead. Handle this properly by showing a message and recreate the
            // session at the next iteration.
            messageSnackbarHelper.showError(this, "Camera not available. Please restart the app.")
            session = null
            return
        }

        surfaceView!!.onResume()
        displayRotationHelper!!.onResume()

        messageSnackbarHelper.showMessage(this, "Please scan and select at least 3 walls.")
    }

    public override fun onPause() {
        super.onPause()
        if (session != null) {
            // Note that the order matters - GLSurfaceView is paused first so that it does not try
            // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
            // still call session.update() and get a SessionPausedException.
            displayRotationHelper!!.onPause()
            surfaceView!!.onPause()
            session!!.pause()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, results: IntArray) {
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                .show()
            if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                CameraPermissionHelper.launchPermissionSettings(this)
            }
            finish()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        FullScreenHelper.setFullScreenOnWindowFocusChanged(this, hasFocus)
    }

    override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f)

        // Prepare the rendering objects. This involves reading shaders, so may throw an IOException.
        try {
            // Create the texture and pass it to ARCore session to be filled during update().
            backgroundRenderer.createOnGlThread(this)
            planeRenderer.createOnGlThread(this, "models/trigrid.png")
            pointCloudRenderer.createOnGlThread(this)

            mapObject.createOnGlThread(this, "models/plane.obj", mapFilename!!, isTexturePlane = true)
            mapObject.setMaterialProperties(0.0f, 1.0f, 0.0f, 6.0f)

            pinObject.createOnGlThread(this, "models/pin.obj", "models/pin.png")
            pinObject.setMaterialProperties(0.0f, 1.0f, 0.5f, 6.0f)

            markObject.createOnGlThread(this, "models/pin.obj", "models/green_pin.png")
            markObject.setMaterialProperties(0.0f, 1.0f, 0.5f, 6.0f)

            pinShadowObject.createOnGlThread(this, "models/plane.obj", "models/pin_shadow.png")
            pinShadowObject.setBlendMode(BlendMode.Shadow)
            pinShadowObject.setMaterialProperties(1.0f, 0.0f, 0.0f, 1.0f)
        } catch (e: IOException) {
            Log.e(TAG, "Failed to read an asset file", e)
        }

    }

    override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
        displayRotationHelper!!.onSurfaceChanged(width, height)
        GLES20.glViewport(0, 0, width, height)
    }

    override fun onDrawFrame(gl: GL10) {
        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)

        if (session == null) {
            return
        }
        // Notify ARCore session that the view size changed so that the perspective matrix and
        // the video background can be properly adjusted.
        displayRotationHelper!!.updateSessionIfNeeded(session!!)

        try {
            session!!.setCameraTextureName(backgroundRenderer.textureId)

            // Obtain the current frame from ARSession. When the configuration is set to
            // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
            // camera framerate.
            val frame = session!!.update()
            val camera = frame.camera

            // Handle taps. Handling only one tap per frame, as taps are usually low frequency
            // compared to frame rate.
            val tap = tapHelper!!.poll()
            if (tap != null && camera.trackingState == TrackingState.TRACKING) {
                for (hit in frame.hitTest(tap)) {
                    // Check if any plane was hit, and if it was hit inside the plane polygon
                    val trackable = hit.trackable
                    // Creates an anchor if a plane or an oriented point was hit.
                    if ((trackable is Plane
                            && trackable.isPoseInPolygon(hit.hitPose)
                            && PlaneRenderer.calculateDistanceToPlane(hit.hitPose, camera.pose) > 0)
                        || trackable is Point && trackable.orientationMode == OrientationMode.ESTIMATED_SURFACE_NORMAL) {
                        // Hits are sorted by depth. Consider only closest hit on a plane or oriented point.
                        // Cap the number of objects created. This avoids overloading both the
                        // rendering system and ARCore.

                        // Handle tap event in a dialog here:
                        handleNewWall()

                        // Adding an Anchor tells ARCore that it should track this position in
                        // space. This anchor is created on the Plane to place the 3D model
                        // in the correct position relative both to the world and to the plane.
                        tmpAnchor = hit.createAnchor()
                        break
                    }
                }
            }

            val numMarks = wallSolver.numMarks()
            if(matricies.size > anchors.size) {
                if (tmpAnchor != null) {
                    anchors.add(tmpAnchor!!)
                    tmpAnchor = null
                } else {
                    matricies.removeAt(matricies.size - 1)
                }

                val transform = matricies[numMarks]
                var mtx = Matrix()
                transform.invert(mtx)
                Log.i("info_app", "New matrix ${transform} has inverse ${mtx}")
                var pts = floatArrayOf(canvasWidth / 2f, canvasHight / 2f, canvasWidth / 2f, 0f)
                mtx.mapPoints(pts)
                Log.i("info_app", "Transformed ${pts.toList()} to ${numMarks} marks")

                val halfSize = mapObject.texSize / 2
                val halfWidth = mapObject.texWidth / 2
                val halfHeight = mapObject.texHeight / 2
                val x0 = (pts[0].toDouble() - halfWidth) / halfSize
                val y0 = (pts[1].toDouble() - halfHeight) / halfSize
                val nx = (pts[2].toDouble() - halfWidth) / halfSize - x0
                val ny = (pts[3].toDouble() - halfHeight) / halfSize - y0


                wallSolver.addWall(x0, y0, nx, ny)
                wallSolver.updateSolver()
            }

            // Draw background.
            backgroundRenderer.draw(frame)

            // If not tracking, don't draw 3d objects.
            if (camera.trackingState == TrackingState.PAUSED) {
                return
            }

            // Get projection matrix.
            val projmtx = FloatArray(16)
            camera.getProjectionMatrix(projmtx, 0, 0.1f, 100.0f)

            // Get camera matrix and draw.
            val viewmtx = FloatArray(16)
            camera.getViewMatrix(viewmtx, 0)

            // Compute lighting from average intensity of the image.
            // The first three components are color scaling factors.
            // The last one is the average pixel intensity in gamma space.
            val colorCorrectionRgba = FloatArray(4)
            frame.lightEstimate.getColorCorrection(colorCorrectionRgba, 0)

            // Visualize tracked points.
            val pointCloud = frame.acquirePointCloud()
            pointCloudRenderer.update(pointCloud)
            pointCloudRenderer.draw(viewmtx, projmtx)

            // Application is responsible for releasing the point cloud resources after
            // using it.
            pointCloud.release()

            // Check if we detected at least one plane. If so, hide the loading message.
            if (messageSnackbarHelper.isShowing) {
                if (wallSolver.numMarks() >= 3 && anchors.size >= 3 && !reversed) {
                    messageSnackbarHelper.hide(this)
                }
            }

            // Visualize planes.
            planeRenderer.drawPlanes(
                session!!.getAllTrackables(Plane::class.java), camera.displayOrientedPose, projmtx)

            // solve for position
            var distances = DoubleArray(anchors.size)
            for (i in 0 until anchors.size) {
                distances[i] = distanceAlong(camera.displayOrientedPose, anchors[i].pose)
            }
            val mapXY = wallSolver.solve(distances)

            val offsetCamera = Pose.makeTranslation(1f, 0f, 0f).compose(camera.displayOrientedPose)
            for (i in 0 until anchors.size) {
                distances[i] = distanceAlong(offsetCamera, anchors[i].pose)
            }
            val mapXY2 = wallSolver.solve(distances)

            // render map
            var mapX = 0f
            var mapY = 0f
            var rotate = 0f
            var showPin = false
            if (mapXY != null && mapXY2 != null) {
                mapX = mapXY.first.toFloat()
                mapY = mapXY.second.toFloat()

                val dx = mapXY2.first - mapXY.first
                val dy = mapXY2.second - mapXY.second
                val dmag = sqrt(dx * dx + dy * dy)
                rotate = atan2(dy / dmag, dx / dmag).toFloat()

                showPin = true
            }

            if (camera.displayOrientedPose.zAxis[1] < -0.9) {
                if (!reversed && anchors.size > 0 && matricies.size > 0) {
                    anchors.removeAt(anchors.size - 1)
                    matricies.removeAt(matricies.size - 1)
                    messageSnackbarHelper.showMessage(this, "Removed marker.")
                }
                reversed = true
            } else {
                reversed = false
            }


            val mapScale = 0.5f
            val posePin = camera.displayOrientedPose
                    .compose(Pose.makeTranslation(0f, 0f, -2f * mapScale))
                    .extractTranslation()
                    .compose(Pose.makeTranslation(0f, -0.2f * mapScale, 0f))
                    .compose(axisAngleToPose(0.0f, 1.0f, 0.0f, rotate + extraRotate))

            if (showPin) {
                posePin.toMatrix(tmpMatrix, 0)
                pinObject.updateModelMatrix(tmpMatrix, mapScale * 0.15f)
                pinShadowObject.updateModelMatrix(tmpMatrix, mapScale * 0.15f)
                pinObject.draw(viewmtx, projmtx, colorCorrectionRgba)
                pinShadowObject.draw(viewmtx, projmtx, colorCorrectionRgba)
            }

            posePin.compose(Pose.makeTranslation(-mapX * mapScale, -mapScale * 0.02f, -mapY * mapScale)).toMatrix(tmpMatrix, 0)
            mapObject.updateModelMatrix(tmpMatrix, mapScale)
            mapObject.draw(viewmtx, projmtx, colorCorrectionRgba)

            // Visualize anchors created by touch.
            val scaleFactor = 0.2f
            for (anchor in anchors) {
                if (anchor.trackingState != TrackingState.TRACKING) {
                    continue
                }
                // Get the current pose of an Anchor in world space. The Anchor pose is updated
                // during calls to session.update() as ARCore refines its estimate of the world.
                anchor.pose.toMatrix(tmpMatrix, 0)

                // Update and draw the model and its shadow.
                markObject.updateModelMatrix(tmpMatrix, scaleFactor)
                pinShadowObject.updateModelMatrix(tmpMatrix, scaleFactor)
                markObject.draw(viewmtx, projmtx, colorCorrectionRgba)
                pinShadowObject.draw(viewmtx, projmtx, colorCorrectionRgba)
            }

        } catch (t: Throwable) {
            // Avoid crashing the application due to unhandled exceptions.
            Log.e(TAG, "Exception on the OpenGL thread", t)
        }

    }

    fun addMatrix(matrix: Matrix) {
        matricies.add(matrix)

        var fragmentManager: FragmentManager = supportFragmentManager
        var fragment = fragmentManager.findFragmentByTag("MATRIX")
        var fragmentTransaction=fragmentManager.beginTransaction()
        fragmentTransaction.remove(fragment)
        fragmentTransaction.commit()

    }

    val FRAGMENT_ID = 55

    fun handleNewWall() {
//        val manager = fragmentManager
//        val transaction = manager.beginTransaction()
//        var bundle = Bundle()
////        bundle.putSerializable("path", mapFilename)
////        val fragment = align_photo
//        var fragment = AlignPhoto.newInstance(mapFilename!!)
//        fragment.arguments = bundle
//        transaction.add(FRAGMENT_ID, fragment as Fragment)
//        transaction.commit()
        var fragmentManager: FragmentManager = supportFragmentManager
        var fragmentTransaction=fragmentManager.beginTransaction()
        var myFrag: Fragment = AlignPhoto.newInstance("")
        fragmentTransaction.add(R.id.constraintLayout, myFrag, "MATRIX")
        fragmentTransaction.commit()
    }

    fun isBetterLocation(location: Location, currentBestLocation: Location?): Boolean {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true
        }

        // Check whether the new location fix is newer or older
        val timeDelta: Long = location.time - currentBestLocation.time
        val isSignificantlyNewer: Boolean = timeDelta > TIME_DELTA
        val isSignificantlyOlder:Boolean = timeDelta < -TIME_DELTA

        when {
            // If it's been more than two minutes since the current location, use the new location
            // because the user has likely moved
            isSignificantlyNewer -> return true
            // If the new location is more than two minutes older, it must be worse
            isSignificantlyOlder -> return false
        }

        // Check whether the new location fix is more or less accurate
        val isNewer: Boolean = timeDelta > 0L
        val accuracyDelta: Float = location.accuracy - currentBestLocation.accuracy
        val isLessAccurate: Boolean = accuracyDelta > 0f
        val isMoreAccurate: Boolean = accuracyDelta < 0f
        val isSignificantlyLessAccurate: Boolean = accuracyDelta > 200f

        // Check if the old and new location are from the same provider
        val isFromSameProvider: Boolean = location.provider == currentBestLocation.provider

        // Determine location quality using a combination of timeliness and accuracy
        return when {
            isMoreAccurate -> true
            isNewer && !isLessAccurate -> true
            isNewer && !isSignificantlyLessAccurate && isFromSameProvider -> true
            else -> false
        }
    }

    fun fileName(): String {
        return mapFilename!!
    }

//    fun


    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                var dialog = apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                dialog.show()
            } else {
                Log.i("info_app_location", "This device is not supported.")
                finish()
            }
            return false
        }
        return true
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}
