package edu.mines.armap

private val x_dlat = -0.0007052631578942151 * 0.5
private val x_dlon = 0.00036000000001853267 * 0.5
private val y_dlat = -0.00028624078623387786 * 0.5
private val y_dlon = -0.000855036855025435 * 0.5
private val lat0 = 37.48648208179232 + x_dlat + y_dlat
private val lon0 = -122.14672083243245 + x_dlon + y_dlon

private val x_mag2 = x_dlat * x_dlat + x_dlon * x_dlon
private val y_mag2 = y_dlat * y_dlat + y_dlon * y_dlon

private val x_hlat = x_dlat / x_mag2
private val x_hlon = x_dlon / x_mag2
private val y_hlat = y_dlat / y_mag2
private val y_hlon = y_dlon / y_mag2

fun builtinMapXY(lat: Double, lon: Double): Pair<Double, Double> {
    val dlat = lat - lat0
    val dlon = lon - lon0
    return Pair(dlat * x_hlat + dlon * x_hlon, dlat * y_hlat + dlon * y_hlon)
}