package edu.mines.armap.models

import android.media.Image

data class MapModel(val name: String, val imageUri: String)
